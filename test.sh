#!/bin/bash -eux

# Check for the page title
curl localhost:$1 | grep "A Really Boring Page"

# Test for the teacher
curl localhost:$1 | grep Aaron
curl localhost:$1 | grep "Chris Anderson"
curl localhost:$1 | grep -v "Bill Gates"

# Test for the students
curl localhost:$1 | grep "John Lennon"
curl localhost:$1 | grep "Ringo Starr"
curl localhost:$1 | grep "Paul McCartney"
curl localhost:$1 | grep "George Harrison"
curl localhost:$1 | grep "Kirk Hammet"
curl localhost:$1 | grep -v "Brittany Spears"
curl localhost:$1 | grep "Jimi Hendrix"
curl localhost:$1 | grep "Bart Simpson"
